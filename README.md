# CERN IT Monitoring Mimir Helm Chart

This repository contains the Helm chart `cern-monit-it-mimir`, which is used by the IT Monitoring Service to install and configure required components of Grafana Mimir.

## Chart Information

- **Chart Name:** cern-monit-it-mimir
- **Chart Version:** 0.1.0
- **App Version:** v0.1.0
- **Kubernetes Version:** >=1.28.0-0
- **Description:** Helm Chart used by IT Monitoring Service to install and configure required components of Grafana Mimir.

## Requirements

- Kubernetes >= 1.28.0

## Installation

To install the chart with the release name `my-release`:

```bash
helm repo add my-repo https://example.com/helm-charts
helm install my-release my-repo/cern-monit-it-mimir
```

## Configuration

The following table lists the configurable parameters of the `cern-monit-it-mimir` chart and their default values.

| Parameter| Description | Default |
| -------- | ----------- | ------- |
|`dnsAliases`|List of DNS aliases. For each one, an ingress and its Let's Encrypt certificate will be created.|`[]`|
|`tenants` | List of tenants with write-read access to Monit Mimir. Each tenant has to have a name and password. The password must be in htpasswd compatible format.|`[]`|

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example:

```yaml
dnsAliases: []
tenants: []
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example:

```yaml
dnsAliases:
  - alias1.example.com
  - alias2.example.com
tenants:
  - name: tenant1
    password: tenant1password
  - name: tenant2
    password: tenant2password
```

Specific values to configure the dependencies can be passed to them referencing their key like:

```yaml
consul:
  client:
    enabled: false
mimir:
  alertmanager: {}
certmanager: {}
```

## Dependencies

The cern-monit-it-mimir chart has the following dependencies:

- mimir-distributed (alias: mimir):
Repository: https://grafana.github.io/helm-charts
Version: 5.2.3

- consul:
Repository: https://helm.releases.hashicorp.com
Version: 1.5.0

- cert-manager:
Repository: https://charts.jetstack.io
Version: 1.15.1
